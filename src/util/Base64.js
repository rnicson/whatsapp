export class Base64 {

    static getMimetype(urlBase64) {

        let regex = /^data:(.+);base64,(.*)$/; // expressão regular
        let result = urlBase64.match(regex); // cria um array com três elementos; queremos recuperar o de índice 1
        return result[1];

    }

    static toFile(urlBase64) {

        let mimeType = Base64.getMimetype(urlBase64);
        let ext = mimeType.split('/')[1]; // extrai a extensão do arquivo
        let filename = `file${Date.now()}.${ext}`; // nome do arquivo

        return fetch(urlBase64)
            .then(res => { return res.arrayBuffer(); })
            .then(buffer => { return new File([buffer], filename, { type: mimeType }); });

    }

}